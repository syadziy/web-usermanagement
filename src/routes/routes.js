export const Routes = {
  // auth
  Login: { path: "/login" },

  // content
  Dashboard: { path: "/" },
  Profile: { path: "/profile" },

  // user
  User: { path: "/user" },

  // error page
  Page500: { path: "/server_error" },
  Page404: { path: "*" },
};
