import React, { Component } from "react";
import moment from "moment-timezone";
import { Row, Col, Card } from "@themesberg/react-bootstrap";

export default class footer extends Component {
  render() {
    const currentYear = moment().get("year");

    return (
      <div>
        <footer className="footer section py-5">
          <Row>
            <Col xs={12} lg={6} className="mb-4 mb-lg-0"></Col>
            <Col xs={12} lg={6} className="mb-4 mb-lg-0">
              <p
                className="mb-0 text-center text-xl-right"
                style={{ fontSize: "12px" }}
              >
                Copyright © 2020-{`${currentYear} `} Volt Pro modified by {` `}
                <Card.Link
                  href="https://gitlab.com/syadziy"
                  target="_blank"
                  className="text-blue text-decoration-none fw-normal"
                >
                  Syadziy .NET
                </Card.Link>
              </p>
            </Col>
          </Row>
        </footer>
      </div>
    );
  }
}
