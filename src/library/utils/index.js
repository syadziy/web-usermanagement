import React, { useState, useEffect } from "react";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { makeStyles } from "@material-ui/core/styles";
import Fade from "@material-ui/core/Fade";
import Grid from "@material-ui/core/Grid";
import { Typography, Avatar } from "@material-ui/core";
// import Images from "../assets/Images";
import { Link, useRouteMatch } from "react-router-dom";
import Constant from "../constant/index";

const useStyles = makeStyles({
  avatar: {
    margin: 10,
  },
  orangeAvatar: {
    margin: 10,
    color: "#fff",
    backgroundColor: "black",
  },
});

function FadeMenu() {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLogout = () => {
    localStorage.removeItem(Constant.TOKEN);
    window.location.reload();
    setAnchorEl(null);
  };

  const classes = useStyles();

  let { path, url } = useRouteMatch();

  return (
    <div>
      <Grid container justify="center" alignItems="center">
        <div onClick={handleClick}>
          <img
            // src={Images.dropdownWhite}
            alt="React Logo"
            style={{ marginLeft: 15 }}
          />
        </div>
      </Grid>
      <Menu
        id="fade-menu"
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={handleClose}
        TransitionComponent={Fade}
      >
        <MenuItem disabled={true} style={{ opacity: 1 }}>
          <div style={{ display: "flex", alignItems: "center" }}>
            <Avatar className={classes.orangeAvatar}>T</Avatar>
            <div>
              <Typography>Tommy</Typography>
              <Typography>tom_my@gmail.com</Typography>
            </div>
          </div>
        </MenuItem>
        <MenuItem onClick={handleClose}>
          <Link to={"/home/profile"}>
            <div style={{ display: "flex", padding: 10 }}>
              <img
                // src={Images.setting}
                style={{ height: 20, width: 20, marginRight: 20 }}
              />
              <Typography style={{ color: "black", textDecoration: "none" }}>
                Settings
              </Typography>
            </div>
          </Link>
        </MenuItem>
        <MenuItem onClick={handleLogout}>
          <div style={{ display: "flex", padding: 10 }}>
            <img
              //   src={Images.logout}
              style={{ height: 20, width: 20, marginRight: 20 }}
            />
            <Typography>Logout</Typography>
          </div>
        </MenuItem>
      </Menu>
    </div>
  );
}

const footerStyle = {
  backgroundColor: "#0d2846",
  fontSize: "20px",
  color: "white",
  // borderTop: "1px solid #E7E7E7",
  textAlign: "center",
  padding: "20px",
  left: 0,
  bottom: 0,
  right: 0,
  height: "80px",
  width: "100%",
};

const phantomStyle = {
  display: "block",
};

function Footer({ children }) {
  return (
    <div>
      {/* <div style={phantomStyle} /> */}
      <div style={footerStyle}>{children}</div>
    </div>
  );
}

function getWindowDimensions() {
  const { innerWidth: width, innerHeight: height } = window;
  return {
    width,
    height,
  };
}

function UseWindowDimensions() {
  const [windowDimensions, setWindowDimensions] = useState(
    getWindowDimensions()
  );

  useEffect(() => {
    function handleResize() {
      setWindowDimensions(getWindowDimensions());
    }

    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return windowDimensions;
}

export { FadeMenu, Footer, UseWindowDimensions };
