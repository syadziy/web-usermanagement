import React, { useState, useEffect } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import { Routes } from "../routes/routes";
import Constant from "../library/constant/index";

// components
import Sidebar from "./layouts/sidebar";
import Navbar from "./layouts/navbar";
import Footer from "./layouts/footer";
import Preloader from "./layouts/preloader";

// page
import Login from "../container/auth/index";
import Dashboard from "../container/dashboard/index";
import Profile from "../container/profile/index";

// content
import User from "../container/user/index";

// error page
import Page404 from "../container/errorpage/notfound";
import Page500 from "../container/errorpage/servererror";

const RouteWithLoader = ({ component: Component, ...rest }) => {
  const [loaded, setLoaded] = useState(false);
  const logged = localStorage.getItem(Constant.TOKEN) !== null ? true : false;

  useEffect(() => {
    const timer = setTimeout(() => setLoaded(true), 1000);
    return () => clearTimeout(timer);
  }, []);

  return logged ? (
    <Redirect
      to={{
        pathname: "/",
      }}
    />
  ) : (
    <Route
      {...rest}
      render={(props) => (
        <>
          {" "}
          <Preloader show={loaded ? false : true} /> <Component {...props} />{" "}
        </>
      )}
    />
  );
};

const RouteWithSidebar = ({ component: Component, ...rest }) => {
  const [loaded, setLoaded] = useState(false);
  const logged = localStorage.getItem(Constant.TOKEN) !== null ? true : false;

  useEffect(() => {
    const timer = setTimeout(() => setLoaded(true), 1000);
    return () => clearTimeout(timer);
  }, []);

  const localStorageIsSettingsVisible = () => {
    return localStorage.getItem("settingsVisible") === "false" ? false : true;
  };

  const [showSettings, setShowSettings] = useState(
    localStorageIsSettingsVisible
  );

  const toggleSettings = () => {
    setShowSettings(!showSettings);
    localStorage.setItem("settingsVisible", !showSettings);
  };

  return logged ? (
    <Route
      {...rest}
      render={(props) => (
        <>
          <Preloader show={loaded ? false : true} />
          <Sidebar />

          <main className="content">
            <Navbar />
            <Component {...props} />
            <Footer
              toggleSettings={toggleSettings}
              showSettings={showSettings}
            />
          </main>
        </>
      )}
    />
  ) : (
    <Redirect
      to={{
        pathname: "/login",
      }}
    />
  );
};

export default () => (
  <Switch>
    {/* Auth */}
    <RouteWithLoader exact path={Routes.Login.path} component={Login} />

    {/* Content */}
    <RouteWithSidebar
      exact
      path={Routes.Dashboard.path}
      component={Dashboard}
    />
    <RouteWithSidebar exact path={Routes.Profile.path} component={Profile} />
    <RouteWithSidebar exact path={Routes.User.path} component={User} />

    {/* Error Page */}
    <RouteWithLoader exact path={Routes.Page500.path} component={Page500} />
    <RouteWithLoader exact path={Routes.Page404.path} component={Page404} />

    <Redirect to={Routes.Page404.path} />
  </Switch>
);
