import "../App.css";
import { BrowserRouter as Router } from "react-router-dom";

import ScrollToTop from "../components/ScrollToTop";
import Routing from "../container/index";

// core styles
import "../scss/volt.scss";

// vendor styles
import "@fortawesome/fontawesome-free/css/all.css";
import "react-datetime/css/react-datetime.css";

function Routes() {
  return (
    <Router basename={process.env.REACT_APP_URL_MAIN_FE}>
      <ScrollToTop />
      <Routing />
    </Router>
  );
}

export default Routes;
